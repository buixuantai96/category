package com.nashtech.service.mapper;

import com.nashtech.domain.*;
import com.nashtech.service.dto.CategoryItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CategoryItem} and its DTO {@link CategoryItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class})
public interface CategoryItemMapper extends EntityMapper<CategoryItemDTO, CategoryItem> {

    @Mapping(source = "category.id", target = "categoryId")
    CategoryItemDTO toDto(CategoryItem categoryItem);

    @Mapping(source = "categoryId", target = "category")
    CategoryItem toEntity(CategoryItemDTO categoryItemDTO);

    default CategoryItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategoryItem categoryItem = new CategoryItem();
        categoryItem.setId(id);
        return categoryItem;
    }
}
