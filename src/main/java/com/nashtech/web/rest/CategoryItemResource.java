package com.nashtech.web.rest;

import com.nashtech.domain.CategoryItem;
import com.nashtech.service.CategoryItemService;
import com.nashtech.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nashtech.domain.CategoryItem}.
 */
@RestController
@RequestMapping("/api")
public class CategoryItemResource {

    private final Logger log = LoggerFactory.getLogger(CategoryItemResource.class);

    private static final String ENTITY_NAME = "categoryCategoryItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CategoryItemService categoryItemService;

    public CategoryItemResource(CategoryItemService categoryItemService) {
        this.categoryItemService = categoryItemService;
    }

    /**
     * {@code POST  /category-items} : Create a new categoryItem.
     *
     * @param categoryItem the categoryItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new categoryItem, or with status {@code 400 (Bad Request)} if the categoryItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/category-items")
    public ResponseEntity<CategoryItem> createCategoryItem(@Valid @RequestBody CategoryItem categoryItem) throws URISyntaxException {
        log.debug("REST request to save CategoryItem : {}", categoryItem);
        if (categoryItem.getId() != null) {
            throw new BadRequestAlertException("A new categoryItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategoryItem result = categoryItemService.save(categoryItem);
        return ResponseEntity.created(new URI("/api/category-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /category-items} : Updates an existing categoryItem.
     *
     * @param categoryItem the categoryItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated categoryItem,
     * or with status {@code 400 (Bad Request)} if the categoryItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the categoryItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/category-items")
    public ResponseEntity<CategoryItem> updateCategoryItem(@Valid @RequestBody CategoryItem categoryItem) throws URISyntaxException {
        log.debug("REST request to update CategoryItem : {}", categoryItem);
        if (categoryItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategoryItem result = categoryItemService.save(categoryItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, categoryItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /category-items} : get all the categoryItems.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of categoryItems in body.
     */
    @GetMapping("/category-items")
    public ResponseEntity<List<CategoryItem>> getAllCategoryItems(Pageable pageable, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of CategoryItems");
        Page<CategoryItem> page = categoryItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder, page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /category-items/:id} : get the "id" categoryItem.
     *
     * @param id the id of the categoryItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the categoryItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/category-items/{id}")
    public ResponseEntity<CategoryItem> getCategoryItem(@PathVariable Long id) {
        log.debug("REST request to get CategoryItem : {}", id);
        Optional<CategoryItem> categoryItem = categoryItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(categoryItem);
    }

    /**
     * {@code DELETE  /category-items/:id} : delete the "id" categoryItem.
     *
     * @param id the id of the categoryItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/category-items/{id}")
    public ResponseEntity<Void> deleteCategoryItem(@PathVariable Long id) {
        log.debug("REST request to delete CategoryItem : {}", id);
        categoryItemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
