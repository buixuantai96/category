package com.nashtech.repository;

import com.nashtech.domain.CategoryItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CategoryItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryItemRepository extends JpaRepository<CategoryItem, Long> {

}
