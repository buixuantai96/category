package com.nashtech.web.rest;

import com.nashtech.CategoryApp;
import com.nashtech.domain.CategoryItem;
import com.nashtech.repository.CategoryItemRepository;
import com.nashtech.service.CategoryItemService;
import com.nashtech.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nashtech.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CategoryItemResource} REST controller.
 */
@SpringBootTest(classes = CategoryApp.class)
public class CategoryItemResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CategoryItemRepository categoryItemRepository;

    @Autowired
    private CategoryItemService categoryItemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCategoryItemMockMvc;

    private CategoryItem categoryItem;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CategoryItemResource categoryItemResource = new CategoryItemResource(categoryItemService);
        this.restCategoryItemMockMvc = MockMvcBuilders.standaloneSetup(categoryItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryItem createEntity(EntityManager em) {
        CategoryItem categoryItem = new CategoryItem()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return categoryItem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryItem createUpdatedEntity(EntityManager em) {
        CategoryItem categoryItem = new CategoryItem()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return categoryItem;
    }

    @BeforeEach
    public void initTest() {
        categoryItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategoryItem() throws Exception {
        int databaseSizeBeforeCreate = categoryItemRepository.findAll().size();

        // Create the CategoryItem
        restCategoryItemMockMvc.perform(post("/api/category-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryItem)))
            .andExpect(status().isCreated());

        // Validate the CategoryItem in the database
        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeCreate + 1);
        CategoryItem testCategoryItem = categoryItemList.get(categoryItemList.size() - 1);
        assertThat(testCategoryItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCategoryItem.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCategoryItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoryItemRepository.findAll().size();

        // Create the CategoryItem with an existing ID
        categoryItem.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoryItemMockMvc.perform(post("/api/category-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryItem)))
            .andExpect(status().isBadRequest());

        // Validate the CategoryItem in the database
        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryItemRepository.findAll().size();
        // set the field null
        categoryItem.setName(null);

        // Create the CategoryItem, which fails.

        restCategoryItemMockMvc.perform(post("/api/category-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryItem)))
            .andExpect(status().isBadRequest());

        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCategoryItems() throws Exception {
        // Initialize the database
        categoryItemRepository.saveAndFlush(categoryItem);

        // Get all the categoryItemList
        restCategoryItemMockMvc.perform(get("/api/category-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoryItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getCategoryItem() throws Exception {
        // Initialize the database
        categoryItemRepository.saveAndFlush(categoryItem);

        // Get the categoryItem
        restCategoryItemMockMvc.perform(get("/api/category-items/{id}", categoryItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(categoryItem.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCategoryItem() throws Exception {
        // Get the categoryItem
        restCategoryItemMockMvc.perform(get("/api/category-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategoryItem() throws Exception {
        // Initialize the database
        categoryItemService.save(categoryItem);

        int databaseSizeBeforeUpdate = categoryItemRepository.findAll().size();

        // Update the categoryItem
        CategoryItem updatedCategoryItem = categoryItemRepository.findById(categoryItem.getId()).get();
        // Disconnect from session so that the updates on updatedCategoryItem are not directly saved in db
        em.detach(updatedCategoryItem);
        updatedCategoryItem
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restCategoryItemMockMvc.perform(put("/api/category-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCategoryItem)))
            .andExpect(status().isOk());

        // Validate the CategoryItem in the database
        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeUpdate);
        CategoryItem testCategoryItem = categoryItemList.get(categoryItemList.size() - 1);
        assertThat(testCategoryItem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryItem.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCategoryItem() throws Exception {
        int databaseSizeBeforeUpdate = categoryItemRepository.findAll().size();

        // Create the CategoryItem

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCategoryItemMockMvc.perform(put("/api/category-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryItem)))
            .andExpect(status().isBadRequest());

        // Validate the CategoryItem in the database
        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCategoryItem() throws Exception {
        // Initialize the database
        categoryItemService.save(categoryItem);

        int databaseSizeBeforeDelete = categoryItemRepository.findAll().size();

        // Delete the categoryItem
        restCategoryItemMockMvc.perform(delete("/api/category-items/{id}", categoryItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<CategoryItem> categoryItemList = categoryItemRepository.findAll();
        assertThat(categoryItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoryItem.class);
        CategoryItem categoryItem1 = new CategoryItem();
        categoryItem1.setId(1L);
        CategoryItem categoryItem2 = new CategoryItem();
        categoryItem2.setId(categoryItem1.getId());
        assertThat(categoryItem1).isEqualTo(categoryItem2);
        categoryItem2.setId(2L);
        assertThat(categoryItem1).isNotEqualTo(categoryItem2);
        categoryItem1.setId(null);
        assertThat(categoryItem1).isNotEqualTo(categoryItem2);
    }
}
